var img_stimuli = [
    {
        "": "0",
        "filename": "task_img/hp__bus6.png",
        "category": "bus",
        "manipulation": "high-pass"
    },
    {
        "": "1",
        "filename": "task_img/lp__banana5.png",
        "category": "banana",
        "manipulation": "low-pass"
    },
    {
        "": "2",
        "filename": "task_img/control_bird9.png",
        "category": "bird",
        "manipulation": "control"
    },
    {
        "": "3",
        "filename": "task_img/heavyclutter_firehydrant1.png",
        "category": "firehydrant",
        "manipulation": "clutter"
    },
    {
        "": "4",
        "filename": "task_img/heavyclutter_building9.png",
        "category": "building",
        "manipulation": "clutter"
    },
    {
        "": "5",
        "filename": "task_img/hp__building10.png",
        "category": "building",
        "manipulation": "high-pass"
    },
    {
        "": "6",
        "filename": "task_img/control_cat8.png",
        "category": "cat",
        "manipulation": "control"
    },
    {
        "": "7",
        "filename": "task_img/hp__banana4.png",
        "category": "banana",
        "manipulation": "high-pass"
    },
    {
        "": "8",
        "filename": "task_img/lp__bird9.png",
        "category": "bird",
        "manipulation": "low-pass"
    },
    {
        "": "9",
        "filename": "task_img/blobhigh_banana2.png",
        "category": "banana",
        "manipulation": "occlusion"
    },
    {
        "": "10",
        "filename": "task_img/blobhigh_banana3.png",
        "category": "banana",
        "manipulation": "occlusion"
    },
    {
        "": "11",
        "filename": "task_img/lp__bird8.png",
        "category": "bird",
        "manipulation": "low-pass"
    },
    {
        "": "12",
        "filename": "task_img/heavyclutter_person1.png",
        "category": "person",
        "manipulation": "clutter"
    },
    {
        "": "13",
        "filename": "task_img/hp__banana5.png",
        "category": "banana",
        "manipulation": "high-pass"
    },
    {
        "": "14",
        "filename": "task_img/control_cat9.png",
        "category": "cat",
        "manipulation": "control"
    },
    {
        "": "15",
        "filename": "task_img/heavyclutter_building8.png",
        "category": "building",
        "manipulation": "clutter"
    },
    {
        "": "16",
        "filename": "task_img/control_bird8.png",
        "category": "bird",
        "manipulation": "control"
    },
    {
        "": "17",
        "filename": "task_img/blobhigh_bus1.png",
        "category": "bus",
        "manipulation": "occlusion"
    },
    {
        "": "18",
        "filename": "task_img/lp__banana4.png",
        "category": "banana",
        "manipulation": "low-pass"
    },
    {
        "": "19",
        "filename": "task_img/hp__bus7.png",
        "category": "bus",
        "manipulation": "high-pass"
    },
    {
        "": "20",
        "filename": "task_img/heavyclutter_bird9.png",
        "category": "bird",
        "manipulation": "clutter"
    },
    {
        "": "21",
        "filename": "task_img/heavyclutter_bus9.png",
        "category": "bus",
        "manipulation": "clutter"
    },
    {
        "": "22",
        "filename": "task_img/hp__bus5.png",
        "category": "bus",
        "manipulation": "high-pass"
    },
    {
        "": "23",
        "filename": "task_img/blobhigh_bird8.png",
        "category": "bird",
        "manipulation": "occlusion"
    },
    {
        "": "24",
        "filename": "task_img/lp__banana6.png",
        "category": "banana",
        "manipulation": "low-pass"
    },
    {
        "": "25",
        "filename": "task_img/blobhigh_bus3.png",
        "category": "bus",
        "manipulation": "occlusion"
    },
    {
        "": "26",
        "filename": "task_img/heavyclutter_firehydrant2.png",
        "category": "firehydrant",
        "manipulation": "clutter"
    },
    {
        "": "27",
        "filename": "task_img/hp__banana7.png",
        "category": "banana",
        "manipulation": "high-pass"
    },
    {
        "": "28",
        "filename": "task_img/heavyclutter_person3.png",
        "category": "person",
        "manipulation": "clutter"
    },
    {
        "": "29",
        "filename": "task_img/blobhigh_banana1.png",
        "category": "banana",
        "manipulation": "occlusion"
    },
    {
        "": "30",
        "filename": "task_img/heavyclutter_person2.png",
        "category": "person",
        "manipulation": "clutter"
    },
    {
        "": "31",
        "filename": "task_img/hp__banana6.png",
        "category": "banana",
        "manipulation": "high-pass"
    },
    {
        "": "32",
        "filename": "task_img/lp__person10.png",
        "category": "person",
        "manipulation": "low-pass"
    },
    {
        "": "33",
        "filename": "task_img/heavyclutter_firehydrant3.png",
        "category": "firehydrant",
        "manipulation": "clutter"
    },
    {
        "": "34",
        "filename": "task_img/blobhigh_bus2.png",
        "category": "bus",
        "manipulation": "occlusion"
    },
    {
        "": "35",
        "filename": "task_img/lp__banana7.png",
        "category": "banana",
        "manipulation": "low-pass"
    },
    {
        "": "36",
        "filename": "task_img/blobhigh_bird9.png",
        "category": "bird",
        "manipulation": "occlusion"
    },
    {
        "": "37",
        "filename": "task_img/hp__bus4.png",
        "category": "bus",
        "manipulation": "high-pass"
    },
    {
        "": "38",
        "filename": "task_img/heavyclutter_bus8.png",
        "category": "bus",
        "manipulation": "clutter"
    },
    {
        "": "39",
        "filename": "task_img/heavyclutter_bird8.png",
        "category": "bird",
        "manipulation": "clutter"
    },
    {
        "": "40",
        "filename": "task_img/lp__firehydrant8.png",
        "category": "firehydrant",
        "manipulation": "low-pass"
    },
    {
        "": "41",
        "filename": "task_img/lp__banana3.png",
        "category": "banana",
        "manipulation": "low-pass"
    },
    {
        "": "42",
        "filename": "task_img/hp__building9.png",
        "category": "building",
        "manipulation": "high-pass"
    },
    {
        "": "43",
        "filename": "task_img/heavyclutter_firehydrant7.png",
        "category": "firehydrant",
        "manipulation": "clutter"
    },
    {
        "": "44",
        "filename": "task_img/heavyclutter_tree9.png",
        "category": "tree",
        "manipulation": "clutter"
    },
    {
        "": "45",
        "filename": "task_img/blobhigh_bus6.png",
        "category": "bus",
        "manipulation": "occlusion"
    },
    {
        "": "46",
        "filename": "task_img/blobhigh_tree8.png",
        "category": "tree",
        "manipulation": "occlusion"
    },
    {
        "": "47",
        "filename": "task_img/blobhigh_bird10.png",
        "category": "bird",
        "manipulation": "occlusion"
    },
    {
        "": "48",
        "filename": "task_img/hp__banana2.png",
        "category": "banana",
        "manipulation": "high-pass"
    },
    {
        "": "49",
        "filename": "task_img/heavyclutter_person6.png",
        "category": "person",
        "manipulation": "clutter"
    },
    {
        "": "50",
        "filename": "task_img/blobhigh_firehydrant10.png",
        "category": "firehydrant",
        "manipulation": "occlusion"
    },
    {
        "": "51",
        "filename": "task_img/blobhigh_banana4.png",
        "category": "banana",
        "manipulation": "occlusion"
    },
    {
        "": "52",
        "filename": "task_img/blobhigh_banana5.png",
        "category": "banana",
        "manipulation": "occlusion"
    },
    {
        "": "53",
        "filename": "task_img/heavyclutter_person7.png",
        "category": "person",
        "manipulation": "clutter"
    },
    {
        "": "54",
        "filename": "task_img/hp__banana3.png",
        "category": "banana",
        "manipulation": "high-pass"
    },
    {
        "": "55",
        "filename": "task_img/control_bus10.png",
        "category": "bus",
        "manipulation": "control"
    },
    {
        "": "56",
        "filename": "task_img/blobhigh_tree9.png",
        "category": "tree",
        "manipulation": "occlusion"
    },
    {
        "": "57",
        "filename": "task_img/blobhigh_bus7.png",
        "category": "bus",
        "manipulation": "occlusion"
    },
    {
        "": "58",
        "filename": "task_img/heavyclutter_tree8.png",
        "category": "tree",
        "manipulation": "clutter"
    },
    {
        "": "59",
        "filename": "task_img/hp__building8.png",
        "category": "building",
        "manipulation": "high-pass"
    },
    {
        "": "60",
        "filename": "task_img/heavyclutter_firehydrant6.png",
        "category": "firehydrant",
        "manipulation": "clutter"
    },
    {
        "": "61",
        "filename": "task_img/hp__bus1.png",
        "category": "bus",
        "manipulation": "high-pass"
    },
    {
        "": "62",
        "filename": "task_img/lp__banana2.png",
        "category": "banana",
        "manipulation": "low-pass"
    },
    {
        "": "63",
        "filename": "task_img/lp__firehydrant9.png",
        "category": "firehydrant",
        "manipulation": "low-pass"
    },
    {
        "": "64",
        "filename": "task_img/lp__bus10.png",
        "category": "bus",
        "manipulation": "low-pass"
    },
    {
        "": "65",
        "filename": "task_img/lp__cat9.png",
        "category": "cat",
        "manipulation": "low-pass"
    },
    {
        "": "66",
        "filename": "task_img/control_tree9.png",
        "category": "tree",
        "manipulation": "control"
    },
    {
        "": "67",
        "filename": "task_img/hp__bus3.png",
        "category": "bus",
        "manipulation": "high-pass"
    },
    {
        "": "68",
        "filename": "task_img/heavyclutter_firehydrant4.png",
        "category": "firehydrant",
        "manipulation": "clutter"
    },
    {
        "": "69",
        "filename": "task_img/blobhigh_bus5.png",
        "category": "bus",
        "manipulation": "occlusion"
    },
    {
        "": "70",
        "filename": "task_img/lp__tree9.png",
        "category": "tree",
        "manipulation": "low-pass"
    },
    {
        "": "71",
        "filename": "task_img/hp__banana1.png",
        "category": "banana",
        "manipulation": "high-pass"
    },
    {
        "": "72",
        "filename": "task_img/control_banana8.png",
        "category": "banana",
        "manipulation": "control"
    },
    {
        "": "73",
        "filename": "task_img/heavyclutter_person5.png",
        "category": "person",
        "manipulation": "clutter"
    },
    {
        "": "74",
        "filename": "task_img/blobhigh_bus10.png",
        "category": "bus",
        "manipulation": "occlusion"
    },
    {
        "": "75",
        "filename": "task_img/blobhigh_banana7.png",
        "category": "banana",
        "manipulation": "occlusion"
    },
    {
        "": "76",
        "filename": "task_img/blobhigh_banana6.png",
        "category": "banana",
        "manipulation": "occlusion"
    },
    {
        "": "77",
        "filename": "task_img/heavyclutter_person4.png",
        "category": "person",
        "manipulation": "clutter"
    },
    {
        "": "78",
        "filename": "task_img/heavyclutter_bus10.png",
        "category": "bus",
        "manipulation": "clutter"
    },
    {
        "": "79",
        "filename": "task_img/control_banana9.png",
        "category": "banana",
        "manipulation": "control"
    },
    {
        "": "80",
        "filename": "task_img/lp__banana10.png",
        "category": "banana",
        "manipulation": "low-pass"
    },
    {
        "": "81",
        "filename": "task_img/lp__tree8.png",
        "category": "tree",
        "manipulation": "low-pass"
    },
    {
        "": "82",
        "filename": "task_img/blobhigh_bus4.png",
        "category": "bus",
        "manipulation": "occlusion"
    },
    {
        "": "83",
        "filename": "task_img/heavyclutter_firehydrant5.png",
        "category": "firehydrant",
        "manipulation": "clutter"
    },
    {
        "": "84",
        "filename": "task_img/lp__building10.png",
        "category": "building",
        "manipulation": "low-pass"
    },
    {
        "": "85",
        "filename": "task_img/hp__bus2.png",
        "category": "bus",
        "manipulation": "high-pass"
    },
    {
        "": "86",
        "filename": "task_img/lp__banana1.png",
        "category": "banana",
        "manipulation": "low-pass"
    },
    {
        "": "87",
        "filename": "task_img/control_tree8.png",
        "category": "tree",
        "manipulation": "control"
    },
    {
        "": "88",
        "filename": "task_img/lp__cat8.png",
        "category": "cat",
        "manipulation": "low-pass"
    },
    {
        "": "89",
        "filename": "task_img/lp__building5.png",
        "category": "building",
        "manipulation": "low-pass"
    },
    {
        "": "90",
        "filename": "task_img/lp__person5.png",
        "category": "person",
        "manipulation": "low-pass"
    },
    {
        "": "91",
        "filename": "task_img/control_banana10.png",
        "category": "banana",
        "manipulation": "control"
    },
    {
        "": "92",
        "filename": "task_img/hp__cat6.png",
        "category": "cat",
        "manipulation": "high-pass"
    },
    {
        "": "93",
        "filename": "task_img/blobhigh_building10.png",
        "category": "building",
        "manipulation": "occlusion"
    },
    {
        "": "94",
        "filename": "task_img/hp__person10.png",
        "category": "person",
        "manipulation": "high-pass"
    },
    {
        "": "95",
        "filename": "task_img/hp__person4.png",
        "category": "person",
        "manipulation": "high-pass"
    },
    {
        "": "96",
        "filename": "task_img/hp__bird1.png",
        "category": "bird",
        "manipulation": "high-pass"
    },
    {
        "": "97",
        "filename": "task_img/heavyclutter_bird10.png",
        "category": "bird",
        "manipulation": "clutter"
    },
    {
        "": "98",
        "filename": "task_img/blobhigh_firehydrant6.png",
        "category": "firehydrant",
        "manipulation": "occlusion"
    },
    {
        "": "99",
        "filename": "task_img/control_bus8.png",
        "category": "bus",
        "manipulation": "control"
    },
    {
        "": "100",
        "filename": "task_img/blobhigh_person2.png",
        "category": "person",
        "manipulation": "occlusion"
    },
    {
        "": "101",
        "filename": "task_img/hp__firehydrant1.png",
        "category": "firehydrant",
        "manipulation": "high-pass"
    },
    {
        "": "102",
        "filename": "task_img/hp__tree4.png",
        "category": "tree",
        "manipulation": "high-pass"
    },
    {
        "": "103",
        "filename": "task_img/heavyclutter_banana1.png",
        "category": "banana",
        "manipulation": "clutter"
    },
    {
        "": "104",
        "filename": "task_img/hp__tree5.png",
        "category": "tree",
        "manipulation": "high-pass"
    },
    {
        "": "105",
        "filename": "task_img/blobhigh_banana10.png",
        "category": "banana",
        "manipulation": "occlusion"
    },
    {
        "": "106",
        "filename": "task_img/blobhigh_person3.png",
        "category": "person",
        "manipulation": "occlusion"
    },
    {
        "": "107",
        "filename": "task_img/control_bus9.png",
        "category": "bus",
        "manipulation": "control"
    },
    {
        "": "108",
        "filename": "task_img/blobhigh_firehydrant7.png",
        "category": "firehydrant",
        "manipulation": "occlusion"
    },
    {
        "": "109",
        "filename": "task_img/hp__person5.png",
        "category": "person",
        "manipulation": "high-pass"
    },
    {
        "": "110",
        "filename": "task_img/blobhigh_cat1.png",
        "category": "cat",
        "manipulation": "occlusion"
    },
    {
        "": "111",
        "filename": "task_img/hp__cat7.png",
        "category": "cat",
        "manipulation": "high-pass"
    },
    {
        "": "112",
        "filename": "task_img/control_building1.png",
        "category": "building",
        "manipulation": "control"
    },
    {
        "": "113",
        "filename": "task_img/lp__person4.png",
        "category": "person",
        "manipulation": "low-pass"
    },
    {
        "": "114",
        "filename": "task_img/lp__building4.png",
        "category": "building",
        "manipulation": "low-pass"
    },
    {
        "": "115",
        "filename": "task_img/lp__building6.png",
        "category": "building",
        "manipulation": "low-pass"
    },
    {
        "": "116",
        "filename": "task_img/lp__person6.png",
        "category": "person",
        "manipulation": "low-pass"
    },
    {
        "": "117",
        "filename": "task_img/control_building3.png",
        "category": "building",
        "manipulation": "control"
    },
    {
        "": "118",
        "filename": "task_img/heavyclutter_cat9.png",
        "category": "cat",
        "manipulation": "clutter"
    },
    {
        "": "119",
        "filename": "task_img/hp__cat5.png",
        "category": "cat",
        "manipulation": "high-pass"
    },
    {
        "": "120",
        "filename": "task_img/lp__bird10.png",
        "category": "bird",
        "manipulation": "low-pass"
    },
    {
        "": "121",
        "filename": "task_img/blobhigh_cat3.png",
        "category": "cat",
        "manipulation": "occlusion"
    },
    {
        "": "122",
        "filename": "task_img/hp__person7.png",
        "category": "person",
        "manipulation": "high-pass"
    },
    {
        "": "123",
        "filename": "task_img/hp__bird2.png",
        "category": "bird",
        "manipulation": "high-pass"
    },
    {
        "": "124",
        "filename": "task_img/blobhigh_firehydrant5.png",
        "category": "firehydrant",
        "manipulation": "occlusion"
    },
    {
        "": "125",
        "filename": "task_img/blobhigh_person1.png",
        "category": "person",
        "manipulation": "occlusion"
    },
    {
        "": "126",
        "filename": "task_img/hp__tree7.png",
        "category": "tree",
        "manipulation": "high-pass"
    },
    {
        "": "127",
        "filename": "task_img/heavyclutter_banana3.png",
        "category": "banana",
        "manipulation": "clutter"
    },
    {
        "": "128",
        "filename": "task_img/hp__firehydrant2.png",
        "category": "firehydrant",
        "manipulation": "high-pass"
    },
    {
        "": "129",
        "filename": "task_img/hp__firehydrant3.png",
        "category": "firehydrant",
        "manipulation": "high-pass"
    },
    {
        "": "130",
        "filename": "task_img/hp__tree6.png",
        "category": "tree",
        "manipulation": "high-pass"
    },
    {
        "": "131",
        "filename": "task_img/heavyclutter_banana2.png",
        "category": "banana",
        "manipulation": "clutter"
    },
    {
        "": "132",
        "filename": "task_img/blobhigh_firehydrant4.png",
        "category": "firehydrant",
        "manipulation": "occlusion"
    },
    {
        "": "133",
        "filename": "task_img/hp__bird3.png",
        "category": "bird",
        "manipulation": "high-pass"
    },
    {
        "": "134",
        "filename": "task_img/hp__person6.png",
        "category": "person",
        "manipulation": "high-pass"
    },
    {
        "": "135",
        "filename": "task_img/blobhigh_cat2.png",
        "category": "cat",
        "manipulation": "occlusion"
    },
    {
        "": "136",
        "filename": "task_img/hp__cat4.png",
        "category": "cat",
        "manipulation": "high-pass"
    },
    {
        "": "137",
        "filename": "task_img/heavyclutter_cat8.png",
        "category": "cat",
        "manipulation": "clutter"
    },
    {
        "": "138",
        "filename": "task_img/control_building2.png",
        "category": "building",
        "manipulation": "control"
    },
    {
        "": "139",
        "filename": "task_img/lp__person7.png",
        "category": "person",
        "manipulation": "low-pass"
    },
    {
        "": "140",
        "filename": "task_img/lp__building7.png",
        "category": "building",
        "manipulation": "low-pass"
    },
    {
        "": "141",
        "filename": "task_img/control_person10.png",
        "category": "person",
        "manipulation": "control"
    },
    {
        "": "142",
        "filename": "task_img/lp__person3.png",
        "category": "person",
        "manipulation": "low-pass"
    },
    {
        "": "143",
        "filename": "task_img/lp__building3.png",
        "category": "building",
        "manipulation": "low-pass"
    },
    {
        "": "144",
        "filename": "task_img/control_building6.png",
        "category": "building",
        "manipulation": "control"
    },
    {
        "": "145",
        "filename": "task_img/control_cat10.png",
        "category": "cat",
        "manipulation": "control"
    },
    {
        "": "146",
        "filename": "task_img/blobhigh_cat6.png",
        "category": "cat",
        "manipulation": "occlusion"
    },
    {
        "": "147",
        "filename": "task_img/hp__person2.png",
        "category": "person",
        "manipulation": "high-pass"
    },
    {
        "": "148",
        "filename": "task_img/hp__banana10.png",
        "category": "banana",
        "manipulation": "high-pass"
    },
    {
        "": "149",
        "filename": "task_img/hp__bird7.png",
        "category": "bird",
        "manipulation": "high-pass"
    },
    {
        "": "150",
        "filename": "task_img/lp__cat10.png",
        "category": "cat",
        "manipulation": "low-pass"
    },
    {
        "": "151",
        "filename": "task_img/control_bird10.png",
        "category": "bird",
        "manipulation": "control"
    },
    {
        "": "152",
        "filename": "task_img/blobhigh_person4.png",
        "category": "person",
        "manipulation": "occlusion"
    },
    {
        "": "153",
        "filename": "task_img/heavyclutter_banana6.png",
        "category": "banana",
        "manipulation": "clutter"
    },
    {
        "": "154",
        "filename": "task_img/hp__tree2.png",
        "category": "tree",
        "manipulation": "high-pass"
    },
    {
        "": "155",
        "filename": "task_img/hp__firehydrant7.png",
        "category": "firehydrant",
        "manipulation": "high-pass"
    },
    {
        "": "156",
        "filename": "task_img/hp__firehydrant6.png",
        "category": "firehydrant",
        "manipulation": "high-pass"
    },
    {
        "": "157",
        "filename": "task_img/heavyclutter_banana7.png",
        "category": "banana",
        "manipulation": "clutter"
    },
    {
        "": "158",
        "filename": "task_img/hp__tree3.png",
        "category": "tree",
        "manipulation": "high-pass"
    },
    {
        "": "159",
        "filename": "task_img/blobhigh_person5.png",
        "category": "person",
        "manipulation": "occlusion"
    },
    {
        "": "160",
        "filename": "task_img/blobhigh_person10.png",
        "category": "person",
        "manipulation": "occlusion"
    },
    {
        "": "161",
        "filename": "task_img/hp__bird6.png",
        "category": "bird",
        "manipulation": "high-pass"
    },
    {
        "": "162",
        "filename": "task_img/blobhigh_firehydrant1.png",
        "category": "firehydrant",
        "manipulation": "occlusion"
    },
    {
        "": "163",
        "filename": "task_img/hp__person3.png",
        "category": "person",
        "manipulation": "high-pass"
    },
    {
        "": "164",
        "filename": "task_img/blobhigh_cat7.png",
        "category": "cat",
        "manipulation": "occlusion"
    },
    {
        "": "165",
        "filename": "task_img/control_building7.png",
        "category": "building",
        "manipulation": "control"
    },
    {
        "": "166",
        "filename": "task_img/hp__cat1.png",
        "category": "cat",
        "manipulation": "high-pass"
    },
    {
        "": "167",
        "filename": "task_img/lp__building2.png",
        "category": "building",
        "manipulation": "low-pass"
    },
    {
        "": "168",
        "filename": "task_img/lp__person2.png",
        "category": "person",
        "manipulation": "low-pass"
    },
    {
        "": "169",
        "filename": "task_img/heavyclutter_cat10.png",
        "category": "cat",
        "manipulation": "clutter"
    },
    {
        "": "170",
        "filename": "task_img/lp__bus9.png",
        "category": "bus",
        "manipulation": "low-pass"
    },
    {
        "": "171",
        "filename": "task_img/hp__cat3.png",
        "category": "cat",
        "manipulation": "high-pass"
    },
    {
        "": "172",
        "filename": "task_img/control_building5.png",
        "category": "building",
        "manipulation": "control"
    },
    {
        "": "173",
        "filename": "task_img/blobhigh_cat5.png",
        "category": "cat",
        "manipulation": "occlusion"
    },
    {
        "": "174",
        "filename": "task_img/hp__person1.png",
        "category": "person",
        "manipulation": "high-pass"
    },
    {
        "": "175",
        "filename": "task_img/control_person8.png",
        "category": "person",
        "manipulation": "control"
    },
    {
        "": "176",
        "filename": "task_img/blobhigh_firehydrant3.png",
        "category": "firehydrant",
        "manipulation": "occlusion"
    },
    {
        "": "177",
        "filename": "task_img/hp__bird4.png",
        "category": "bird",
        "manipulation": "high-pass"
    },
    {
        "": "178",
        "filename": "task_img/blobhigh_person7.png",
        "category": "person",
        "manipulation": "occlusion"
    },
    {
        "": "179",
        "filename": "task_img/blobhigh_building9.png",
        "category": "building",
        "manipulation": "occlusion"
    },
    {
        "": "180",
        "filename": "task_img/hp__firehydrant4.png",
        "category": "firehydrant",
        "manipulation": "high-pass"
    },
    {
        "": "181",
        "filename": "task_img/hp__tree1.png",
        "category": "tree",
        "manipulation": "high-pass"
    },
    {
        "": "182",
        "filename": "task_img/control_firehydrant9.png",
        "category": "firehydrant",
        "manipulation": "control"
    },
    {
        "": "183",
        "filename": "task_img/heavyclutter_banana5.png",
        "category": "banana",
        "manipulation": "clutter"
    },
    {
        "": "184",
        "filename": "task_img/control_firehydrant8.png",
        "category": "firehydrant",
        "manipulation": "control"
    },
    {
        "": "185",
        "filename": "task_img/heavyclutter_banana4.png",
        "category": "banana",
        "manipulation": "clutter"
    },
    {
        "": "186",
        "filename": "task_img/hp__firehydrant5.png",
        "category": "firehydrant",
        "manipulation": "high-pass"
    },
    {
        "": "187",
        "filename": "task_img/blobhigh_building8.png",
        "category": "building",
        "manipulation": "occlusion"
    },
    {
        "": "188",
        "filename": "task_img/blobhigh_person6.png",
        "category": "person",
        "manipulation": "occlusion"
    },
    {
        "": "189",
        "filename": "task_img/hp__bird5.png",
        "category": "bird",
        "manipulation": "high-pass"
    },
    {
        "": "190",
        "filename": "task_img/blobhigh_firehydrant2.png",
        "category": "firehydrant",
        "manipulation": "occlusion"
    },
    {
        "": "191",
        "filename": "task_img/control_person9.png",
        "category": "person",
        "manipulation": "control"
    },
    {
        "": "192",
        "filename": "task_img/hp__tree10.png",
        "category": "tree",
        "manipulation": "high-pass"
    },
    {
        "": "193",
        "filename": "task_img/blobhigh_cat4.png",
        "category": "cat",
        "manipulation": "occlusion"
    },
    {
        "": "194",
        "filename": "task_img/control_building4.png",
        "category": "building",
        "manipulation": "control"
    },
    {
        "": "195",
        "filename": "task_img/blobhigh_cat10.png",
        "category": "cat",
        "manipulation": "occlusion"
    },
    {
        "": "196",
        "filename": "task_img/hp__cat2.png",
        "category": "cat",
        "manipulation": "high-pass"
    },
    {
        "": "197",
        "filename": "task_img/lp__bus8.png",
        "category": "bus",
        "manipulation": "low-pass"
    },
    {
        "": "198",
        "filename": "task_img/lp__building1.png",
        "category": "building",
        "manipulation": "low-pass"
    },
    {
        "": "199",
        "filename": "task_img/lp__person1.png",
        "category": "person",
        "manipulation": "low-pass"
    },
    {
        "": "200",
        "filename": "task_img/lp__bus5.png",
        "category": "bus",
        "manipulation": "low-pass"
    },
    {
        "": "201",
        "filename": "task_img/heavyclutter_cat3.png",
        "category": "cat",
        "manipulation": "clutter"
    },
    {
        "": "202",
        "filename": "task_img/control_building9.png",
        "category": "building",
        "manipulation": "control"
    },
    {
        "": "203",
        "filename": "task_img/blobhigh_cat9.png",
        "category": "cat",
        "manipulation": "occlusion"
    },
    {
        "": "204",
        "filename": "task_img/control_tree10.png",
        "category": "tree",
        "manipulation": "control"
    },
    {
        "": "205",
        "filename": "task_img/control_person4.png",
        "category": "person",
        "manipulation": "control"
    },
    {
        "": "206",
        "filename": "task_img/control_bus1.png",
        "category": "bus",
        "manipulation": "control"
    },
    {
        "": "207",
        "filename": "task_img/hp__bird8.png",
        "category": "bird",
        "manipulation": "high-pass"
    },
    {
        "": "208",
        "filename": "task_img/blobhigh_building5.png",
        "category": "building",
        "manipulation": "occlusion"
    },
    {
        "": "209",
        "filename": "task_img/control_firehydrant5.png",
        "category": "firehydrant",
        "manipulation": "control"
    },
    {
        "": "210",
        "filename": "task_img/heavyclutter_banana9.png",
        "category": "banana",
        "manipulation": "clutter"
    },
    {
        "": "211",
        "filename": "task_img/heavyclutter_person10.png",
        "category": "person",
        "manipulation": "clutter"
    },
    {
        "": "212",
        "filename": "task_img/hp__firehydrant8.png",
        "category": "firehydrant",
        "manipulation": "high-pass"
    },
    {
        "": "213",
        "filename": "task_img/hp__firehydrant9.png",
        "category": "firehydrant",
        "manipulation": "high-pass"
    },
    {
        "": "214",
        "filename": "task_img/control_firehydrant4.png",
        "category": "firehydrant",
        "manipulation": "control"
    },
    {
        "": "215",
        "filename": "task_img/heavyclutter_banana8.png",
        "category": "banana",
        "manipulation": "clutter"
    },
    {
        "": "216",
        "filename": "task_img/blobhigh_building4.png",
        "category": "building",
        "manipulation": "occlusion"
    },
    {
        "": "217",
        "filename": "task_img/hp__bird9.png",
        "category": "bird",
        "manipulation": "high-pass"
    },
    {
        "": "218",
        "filename": "task_img/control_person5.png",
        "category": "person",
        "manipulation": "control"
    },
    {
        "": "219",
        "filename": "task_img/blobhigh_cat8.png",
        "category": "cat",
        "manipulation": "occlusion"
    },
    {
        "": "220",
        "filename": "task_img/control_building8.png",
        "category": "building",
        "manipulation": "control"
    },
    {
        "": "221",
        "filename": "task_img/heavyclutter_cat2.png",
        "category": "cat",
        "manipulation": "clutter"
    },
    {
        "": "222",
        "filename": "task_img/lp__bus4.png",
        "category": "bus",
        "manipulation": "low-pass"
    },
    {
        "": "223",
        "filename": "task_img/lp__bus6.png",
        "category": "bus",
        "manipulation": "low-pass"
    },
    {
        "": "224",
        "filename": "task_img/control_person7.png",
        "category": "person",
        "manipulation": "control"
    },
    {
        "": "225",
        "filename": "task_img/control_bus2.png",
        "category": "bus",
        "manipulation": "control"
    },
    {
        "": "226",
        "filename": "task_img/blobhigh_person8.png",
        "category": "person",
        "manipulation": "occlusion"
    },
    {
        "": "227",
        "filename": "task_img/blobhigh_building6.png",
        "category": "building",
        "manipulation": "occlusion"
    },
    {
        "": "228",
        "filename": "task_img/control_firehydrant6.png",
        "category": "firehydrant",
        "manipulation": "control"
    },
    {
        "": "229",
        "filename": "task_img/control_firehydrant7.png",
        "category": "firehydrant",
        "manipulation": "control"
    },
    {
        "": "230",
        "filename": "task_img/blobhigh_building7.png",
        "category": "building",
        "manipulation": "occlusion"
    },
    {
        "": "231",
        "filename": "task_img/blobhigh_person9.png",
        "category": "person",
        "manipulation": "occlusion"
    },
    {
        "": "232",
        "filename": "task_img/control_bus3.png",
        "category": "bus",
        "manipulation": "control"
    },
    {
        "": "233",
        "filename": "task_img/control_person6.png",
        "category": "person",
        "manipulation": "control"
    },
    {
        "": "234",
        "filename": "task_img/heavyclutter_cat1.png",
        "category": "cat",
        "manipulation": "clutter"
    },
    {
        "": "235",
        "filename": "task_img/lp__bus7.png",
        "category": "bus",
        "manipulation": "low-pass"
    },
    {
        "": "236",
        "filename": "task_img/hp__bird10.png",
        "category": "bird",
        "manipulation": "high-pass"
    },
    {
        "": "237",
        "filename": "task_img/lp__bus3.png",
        "category": "bus",
        "manipulation": "low-pass"
    },
    {
        "": "238",
        "filename": "task_img/hp__cat9.png",
        "category": "cat",
        "manipulation": "high-pass"
    },
    {
        "": "239",
        "filename": "task_img/heavyclutter_cat5.png",
        "category": "cat",
        "manipulation": "clutter"
    },
    {
        "": "240",
        "filename": "task_img/hp__bus10.png",
        "category": "bus",
        "manipulation": "high-pass"
    },
    {
        "": "241",
        "filename": "task_img/control_person2.png",
        "category": "person",
        "manipulation": "control"
    },
    {
        "": "242",
        "filename": "task_img/blobhigh_firehydrant9.png",
        "category": "firehydrant",
        "manipulation": "occlusion"
    },
    {
        "": "243",
        "filename": "task_img/control_bus7.png",
        "category": "bus",
        "manipulation": "control"
    },
    {
        "": "244",
        "filename": "task_img/blobhigh_building3.png",
        "category": "building",
        "manipulation": "occlusion"
    },
    {
        "": "245",
        "filename": "task_img/heavyclutter_banana10.png",
        "category": "banana",
        "manipulation": "clutter"
    },
    {
        "": "246",
        "filename": "task_img/heavyclutter_tree10.png",
        "category": "tree",
        "manipulation": "clutter"
    },
    {
        "": "247",
        "filename": "task_img/control_firehydrant3.png",
        "category": "firehydrant",
        "manipulation": "control"
    },
    {
        "": "248",
        "filename": "task_img/control_firehydrant2.png",
        "category": "firehydrant",
        "manipulation": "control"
    },
    {
        "": "249",
        "filename": "task_img/blobhigh_building2.png",
        "category": "building",
        "manipulation": "occlusion"
    },
    {
        "": "250",
        "filename": "task_img/control_bus6.png",
        "category": "bus",
        "manipulation": "control"
    },
    {
        "": "251",
        "filename": "task_img/blobhigh_firehydrant8.png",
        "category": "firehydrant",
        "manipulation": "occlusion"
    },
    {
        "": "252",
        "filename": "task_img/control_person3.png",
        "category": "person",
        "manipulation": "control"
    },
    {
        "": "253",
        "filename": "task_img/heavyclutter_cat4.png",
        "category": "cat",
        "manipulation": "clutter"
    },
    {
        "": "254",
        "filename": "task_img/hp__cat8.png",
        "category": "cat",
        "manipulation": "high-pass"
    },
    {
        "": "255",
        "filename": "task_img/lp__bus2.png",
        "category": "bus",
        "manipulation": "low-pass"
    },
    {
        "": "256",
        "filename": "task_img/lp__building9.png",
        "category": "building",
        "manipulation": "low-pass"
    },
    {
        "": "257",
        "filename": "task_img/lp__person9.png",
        "category": "person",
        "manipulation": "low-pass"
    },
    {
        "": "258",
        "filename": "task_img/heavyclutter_cat6.png",
        "category": "cat",
        "manipulation": "clutter"
    },
    {
        "": "259",
        "filename": "task_img/lp__tree10.png",
        "category": "tree",
        "manipulation": "low-pass"
    },
    {
        "": "260",
        "filename": "task_img/control_person1.png",
        "category": "person",
        "manipulation": "control"
    },
    {
        "": "261",
        "filename": "task_img/hp__person8.png",
        "category": "person",
        "manipulation": "high-pass"
    },
    {
        "": "262",
        "filename": "task_img/control_bus4.png",
        "category": "bus",
        "manipulation": "control"
    },
    {
        "": "263",
        "filename": "task_img/hp__tree8.png",
        "category": "tree",
        "manipulation": "high-pass"
    },
    {
        "": "264",
        "filename": "task_img/hp__tree9.png",
        "category": "tree",
        "manipulation": "high-pass"
    },
    {
        "": "265",
        "filename": "task_img/control_firehydrant1.png",
        "category": "firehydrant",
        "manipulation": "control"
    },
    {
        "": "266",
        "filename": "task_img/blobhigh_building1.png",
        "category": "building",
        "manipulation": "occlusion"
    },
    {
        "": "267",
        "filename": "task_img/control_bus5.png",
        "category": "bus",
        "manipulation": "control"
    },
    {
        "": "268",
        "filename": "task_img/hp__person9.png",
        "category": "person",
        "manipulation": "high-pass"
    },
    {
        "": "269",
        "filename": "task_img/heavyclutter_cat7.png",
        "category": "cat",
        "manipulation": "clutter"
    },
    {
        "": "270",
        "filename": "task_img/lp__person8.png",
        "category": "person",
        "manipulation": "low-pass"
    },
    {
        "": "271",
        "filename": "task_img/lp__building8.png",
        "category": "building",
        "manipulation": "low-pass"
    },
    {
        "": "272",
        "filename": "task_img/lp__bus1.png",
        "category": "bus",
        "manipulation": "low-pass"
    },
    {
        "": "273",
        "filename": "task_img/lp__cat5.png",
        "category": "cat",
        "manipulation": "low-pass"
    },
    {
        "": "274",
        "filename": "task_img/control_tree5.png",
        "category": "tree",
        "manipulation": "control"
    },
    {
        "": "275",
        "filename": "task_img/heavyclutter_bird3.png",
        "category": "bird",
        "manipulation": "clutter"
    },
    {
        "": "276",
        "filename": "task_img/blobhigh_bird2.png",
        "category": "bird",
        "manipulation": "occlusion"
    },
    {
        "": "277",
        "filename": "task_img/lp__firehydrant7.png",
        "category": "firehydrant",
        "manipulation": "low-pass"
    },
    {
        "": "278",
        "filename": "task_img/heavyclutter_bus3.png",
        "category": "bus",
        "manipulation": "clutter"
    },
    {
        "": "279",
        "filename": "task_img/blobhigh_tree10.png",
        "category": "tree",
        "manipulation": "occlusion"
    },
    {
        "": "280",
        "filename": "task_img/heavyclutter_firehydrant8.png",
        "category": "firehydrant",
        "manipulation": "clutter"
    },
    {
        "": "281",
        "filename": "task_img/hp__building6.png",
        "category": "building",
        "manipulation": "high-pass"
    },
    {
        "": "282",
        "filename": "task_img/blobhigh_bus9.png",
        "category": "bus",
        "manipulation": "occlusion"
    },
    {
        "": "283",
        "filename": "task_img/heavyclutter_tree6.png",
        "category": "tree",
        "manipulation": "clutter"
    },
    {
        "": "284",
        "filename": "task_img/blobhigh_tree7.png",
        "category": "tree",
        "manipulation": "occlusion"
    },
    {
        "": "285",
        "filename": "task_img/lp__tree5.png",
        "category": "tree",
        "manipulation": "low-pass"
    },
    {
        "": "286",
        "filename": "task_img/control_banana4.png",
        "category": "banana",
        "manipulation": "control"
    },
    {
        "": "287",
        "filename": "task_img/control_cat1.png",
        "category": "cat",
        "manipulation": "control"
    },
    {
        "": "288",
        "filename": "task_img/heavyclutter_person9.png",
        "category": "person",
        "manipulation": "clutter"
    },
    {
        "": "289",
        "filename": "task_img/heavyclutter_person8.png",
        "category": "person",
        "manipulation": "clutter"
    },
    {
        "": "290",
        "filename": "task_img/lp__bird1.png",
        "category": "bird",
        "manipulation": "low-pass"
    },
    {
        "": "291",
        "filename": "task_img/control_banana5.png",
        "category": "banana",
        "manipulation": "control"
    },
    {
        "": "292",
        "filename": "task_img/heavyclutter_firehydrant10.png",
        "category": "firehydrant",
        "manipulation": "clutter"
    },
    {
        "": "293",
        "filename": "task_img/lp__tree4.png",
        "category": "tree",
        "manipulation": "low-pass"
    },
    {
        "": "294",
        "filename": "task_img/heavyclutter_building1.png",
        "category": "building",
        "manipulation": "clutter"
    },
    {
        "": "295",
        "filename": "task_img/blobhigh_tree6.png",
        "category": "tree",
        "manipulation": "occlusion"
    },
    {
        "": "296",
        "filename": "task_img/heavyclutter_tree7.png",
        "category": "tree",
        "manipulation": "clutter"
    },
    {
        "": "297",
        "filename": "task_img/blobhigh_bus8.png",
        "category": "bus",
        "manipulation": "occlusion"
    },
    {
        "": "298",
        "filename": "task_img/control_bird1.png",
        "category": "bird",
        "manipulation": "control"
    },
    {
        "": "299",
        "filename": "task_img/heavyclutter_firehydrant9.png",
        "category": "firehydrant",
        "manipulation": "clutter"
    },
    {
        "": "300",
        "filename": "task_img/hp__building7.png",
        "category": "building",
        "manipulation": "high-pass"
    },
    {
        "": "301",
        "filename": "task_img/heavyclutter_bus2.png",
        "category": "bus",
        "manipulation": "clutter"
    },
    {
        "": "302",
        "filename": "task_img/blobhigh_bird3.png",
        "category": "bird",
        "manipulation": "occlusion"
    },
    {
        "": "303",
        "filename": "task_img/lp__firehydrant6.png",
        "category": "firehydrant",
        "manipulation": "low-pass"
    },
    {
        "": "304",
        "filename": "task_img/heavyclutter_bird2.png",
        "category": "bird",
        "manipulation": "clutter"
    },
    {
        "": "305",
        "filename": "task_img/control_tree4.png",
        "category": "tree",
        "manipulation": "control"
    },
    {
        "": "306",
        "filename": "task_img/lp__cat4.png",
        "category": "cat",
        "manipulation": "low-pass"
    },
    {
        "": "307",
        "filename": "task_img/lp__cat6.png",
        "category": "cat",
        "manipulation": "low-pass"
    },
    {
        "": "308",
        "filename": "task_img/control_tree6.png",
        "category": "tree",
        "manipulation": "control"
    },
    {
        "": "309",
        "filename": "task_img/lp__firehydrant4.png",
        "category": "firehydrant",
        "manipulation": "low-pass"
    },
    {
        "": "310",
        "filename": "task_img/blobhigh_bird1.png",
        "category": "bird",
        "manipulation": "occlusion"
    },
    {
        "": "311",
        "filename": "task_img/hp__building5.png",
        "category": "building",
        "manipulation": "high-pass"
    },
    {
        "": "312",
        "filename": "task_img/hp__firehydrant10.png",
        "category": "firehydrant",
        "manipulation": "high-pass"
    },
    {
        "": "313",
        "filename": "task_img/heavyclutter_tree5.png",
        "category": "tree",
        "manipulation": "clutter"
    },
    {
        "": "314",
        "filename": "task_img/control_bird3.png",
        "category": "bird",
        "manipulation": "control"
    },
    {
        "": "315",
        "filename": "task_img/blobhigh_tree4.png",
        "category": "tree",
        "manipulation": "occlusion"
    },
    {
        "": "316",
        "filename": "task_img/heavyclutter_building3.png",
        "category": "building",
        "manipulation": "clutter"
    },
    {
        "": "317",
        "filename": "task_img/lp__tree6.png",
        "category": "tree",
        "manipulation": "low-pass"
    },
    {
        "": "318",
        "filename": "task_img/control_banana7.png",
        "category": "banana",
        "manipulation": "control"
    },
    {
        "": "319",
        "filename": "task_img/control_cat2.png",
        "category": "cat",
        "manipulation": "control"
    },
    {
        "": "320",
        "filename": "task_img/lp__bird3.png",
        "category": "bird",
        "manipulation": "low-pass"
    },
    {
        "": "321",
        "filename": "task_img/blobhigh_banana8.png",
        "category": "banana",
        "manipulation": "occlusion"
    },
    {
        "": "322",
        "filename": "task_img/control_building10.png",
        "category": "building",
        "manipulation": "control"
    },
    {
        "": "323",
        "filename": "task_img/blobhigh_banana9.png",
        "category": "banana",
        "manipulation": "occlusion"
    },
    {
        "": "324",
        "filename": "task_img/lp__bird2.png",
        "category": "bird",
        "manipulation": "low-pass"
    },
    {
        "": "325",
        "filename": "task_img/control_cat3.png",
        "category": "cat",
        "manipulation": "control"
    },
    {
        "": "326",
        "filename": "task_img/control_banana6.png",
        "category": "banana",
        "manipulation": "control"
    },
    {
        "": "327",
        "filename": "task_img/lp__tree7.png",
        "category": "tree",
        "manipulation": "low-pass"
    },
    {
        "": "328",
        "filename": "task_img/heavyclutter_building2.png",
        "category": "building",
        "manipulation": "clutter"
    },
    {
        "": "329",
        "filename": "task_img/blobhigh_tree5.png",
        "category": "tree",
        "manipulation": "occlusion"
    },
    {
        "": "330",
        "filename": "task_img/control_bird2.png",
        "category": "bird",
        "manipulation": "control"
    },
    {
        "": "331",
        "filename": "task_img/heavyclutter_tree4.png",
        "category": "tree",
        "manipulation": "clutter"
    },
    {
        "": "332",
        "filename": "task_img/hp__building4.png",
        "category": "building",
        "manipulation": "high-pass"
    },
    {
        "": "333",
        "filename": "task_img/heavyclutter_bus1.png",
        "category": "bus",
        "manipulation": "clutter"
    },
    {
        "": "334",
        "filename": "task_img/lp__firehydrant5.png",
        "category": "firehydrant",
        "manipulation": "low-pass"
    },
    {
        "": "335",
        "filename": "task_img/control_tree7.png",
        "category": "tree",
        "manipulation": "control"
    },
    {
        "": "336",
        "filename": "task_img/heavyclutter_bird1.png",
        "category": "bird",
        "manipulation": "clutter"
    },
    {
        "": "337",
        "filename": "task_img/lp__cat7.png",
        "category": "cat",
        "manipulation": "low-pass"
    },
    {
        "": "338",
        "filename": "task_img/heavyclutter_bird5.png",
        "category": "bird",
        "manipulation": "clutter"
    },
    {
        "": "339",
        "filename": "task_img/control_tree3.png",
        "category": "tree",
        "manipulation": "control"
    },
    {
        "": "340",
        "filename": "task_img/lp__cat3.png",
        "category": "cat",
        "manipulation": "low-pass"
    },
    {
        "": "341",
        "filename": "task_img/hp__bus9.png",
        "category": "bus",
        "manipulation": "high-pass"
    },
    {
        "": "342",
        "filename": "task_img/heavyclutter_bus5.png",
        "category": "bus",
        "manipulation": "clutter"
    },
    {
        "": "343",
        "filename": "task_img/blobhigh_bird4.png",
        "category": "bird",
        "manipulation": "occlusion"
    },
    {
        "": "344",
        "filename": "task_img/lp__firehydrant1.png",
        "category": "firehydrant",
        "manipulation": "low-pass"
    },
    {
        "": "345",
        "filename": "task_img/control_bird6.png",
        "category": "bird",
        "manipulation": "control"
    },
    {
        "": "346",
        "filename": "task_img/heavyclutter_building6.png",
        "category": "building",
        "manipulation": "clutter"
    },
    {
        "": "347",
        "filename": "task_img/blobhigh_tree1.png",
        "category": "tree",
        "manipulation": "occlusion"
    },
    {
        "": "348",
        "filename": "task_img/lp__tree3.png",
        "category": "tree",
        "manipulation": "low-pass"
    },
    {
        "": "349",
        "filename": "task_img/control_banana2.png",
        "category": "banana",
        "manipulation": "control"
    },
    {
        "": "350",
        "filename": "task_img/control_cat7.png",
        "category": "cat",
        "manipulation": "control"
    },
    {
        "": "351",
        "filename": "task_img/control_firehydrant10.png",
        "category": "firehydrant",
        "manipulation": "control"
    },
    {
        "": "352",
        "filename": "task_img/lp__bird6.png",
        "category": "bird",
        "manipulation": "low-pass"
    },
    {
        "": "353",
        "filename": "task_img/lp__bird7.png",
        "category": "bird",
        "manipulation": "low-pass"
    },
    {
        "": "354",
        "filename": "task_img/hp__cat10.png",
        "category": "cat",
        "manipulation": "high-pass"
    },
    {
        "": "355",
        "filename": "task_img/control_cat6.png",
        "category": "cat",
        "manipulation": "control"
    },
    {
        "": "356",
        "filename": "task_img/control_banana3.png",
        "category": "banana",
        "manipulation": "control"
    },
    {
        "": "357",
        "filename": "task_img/lp__tree2.png",
        "category": "tree",
        "manipulation": "low-pass"
    },
    {
        "": "358",
        "filename": "task_img/heavyclutter_building7.png",
        "category": "building",
        "manipulation": "clutter"
    },
    {
        "": "359",
        "filename": "task_img/hp__building1.png",
        "category": "building",
        "manipulation": "high-pass"
    },
    {
        "": "360",
        "filename": "task_img/control_bird7.png",
        "category": "bird",
        "manipulation": "control"
    },
    {
        "": "361",
        "filename": "task_img/heavyclutter_tree1.png",
        "category": "tree",
        "manipulation": "clutter"
    },
    {
        "": "362",
        "filename": "task_img/blobhigh_bird5.png",
        "category": "bird",
        "manipulation": "occlusion"
    },
    {
        "": "363",
        "filename": "task_img/heavyclutter_bus4.png",
        "category": "bus",
        "manipulation": "clutter"
    },
    {
        "": "364",
        "filename": "task_img/hp__bus8.png",
        "category": "bus",
        "manipulation": "high-pass"
    },
    {
        "": "365",
        "filename": "task_img/lp__cat2.png",
        "category": "cat",
        "manipulation": "low-pass"
    },
    {
        "": "366",
        "filename": "task_img/control_tree2.png",
        "category": "tree",
        "manipulation": "control"
    },
    {
        "": "367",
        "filename": "task_img/heavyclutter_bird4.png",
        "category": "bird",
        "manipulation": "clutter"
    },
    {
        "": "368",
        "filename": "task_img/heavyclutter_bird6.png",
        "category": "bird",
        "manipulation": "clutter"
    },
    {
        "": "369",
        "filename": "task_img/heavyclutter_bus6.png",
        "category": "bus",
        "manipulation": "clutter"
    },
    {
        "": "370",
        "filename": "task_img/lp__firehydrant2.png",
        "category": "firehydrant",
        "manipulation": "low-pass"
    },
    {
        "": "371",
        "filename": "task_img/blobhigh_bird7.png",
        "category": "bird",
        "manipulation": "occlusion"
    },
    {
        "": "372",
        "filename": "task_img/lp__banana9.png",
        "category": "banana",
        "manipulation": "low-pass"
    },
    {
        "": "373",
        "filename": "task_img/control_bird5.png",
        "category": "bird",
        "manipulation": "control"
    },
    {
        "": "374",
        "filename": "task_img/heavyclutter_tree3.png",
        "category": "tree",
        "manipulation": "clutter"
    },
    {
        "": "375",
        "filename": "task_img/hp__building3.png",
        "category": "building",
        "manipulation": "high-pass"
    },
    {
        "": "376",
        "filename": "task_img/heavyclutter_building5.png",
        "category": "building",
        "manipulation": "clutter"
    },
    {
        "": "377",
        "filename": "task_img/blobhigh_tree2.png",
        "category": "tree",
        "manipulation": "occlusion"
    },
    {
        "": "378",
        "filename": "task_img/heavyclutter_building10.png",
        "category": "building",
        "manipulation": "clutter"
    },
    {
        "": "379",
        "filename": "task_img/control_banana1.png",
        "category": "banana",
        "manipulation": "control"
    },
    {
        "": "380",
        "filename": "task_img/control_cat4.png",
        "category": "cat",
        "manipulation": "control"
    },
    {
        "": "381",
        "filename": "task_img/hp__banana8.png",
        "category": "banana",
        "manipulation": "high-pass"
    },
    {
        "": "382",
        "filename": "task_img/lp__bird5.png",
        "category": "bird",
        "manipulation": "low-pass"
    },
    {
        "": "383",
        "filename": "task_img/lp__bird4.png",
        "category": "bird",
        "manipulation": "low-pass"
    },
    {
        "": "384",
        "filename": "task_img/hp__banana9.png",
        "category": "banana",
        "manipulation": "high-pass"
    },
    {
        "": "385",
        "filename": "task_img/control_cat5.png",
        "category": "cat",
        "manipulation": "control"
    },
    {
        "": "386",
        "filename": "task_img/lp__tree1.png",
        "category": "tree",
        "manipulation": "low-pass"
    },
    {
        "": "387",
        "filename": "task_img/blobhigh_tree3.png",
        "category": "tree",
        "manipulation": "occlusion"
    },
    {
        "": "388",
        "filename": "task_img/heavyclutter_building4.png",
        "category": "building",
        "manipulation": "clutter"
    },
    {
        "": "389",
        "filename": "task_img/hp__building2.png",
        "category": "building",
        "manipulation": "high-pass"
    },
    {
        "": "390",
        "filename": "task_img/heavyclutter_tree2.png",
        "category": "tree",
        "manipulation": "clutter"
    },
    {
        "": "391",
        "filename": "task_img/control_bird4.png",
        "category": "bird",
        "manipulation": "control"
    },
    {
        "": "392",
        "filename": "task_img/lp__banana8.png",
        "category": "banana",
        "manipulation": "low-pass"
    },
    {
        "": "393",
        "filename": "task_img/lp__firehydrant3.png",
        "category": "firehydrant",
        "manipulation": "low-pass"
    },
    {
        "": "394",
        "filename": "task_img/blobhigh_bird6.png",
        "category": "bird",
        "manipulation": "occlusion"
    },
    {
        "": "395",
        "filename": "task_img/heavyclutter_bus7.png",
        "category": "bus",
        "manipulation": "clutter"
    },
    {
        "": "396",
        "filename": "task_img/lp__firehydrant10.png",
        "category": "firehydrant",
        "manipulation": "low-pass"
    },
    {
        "": "397",
        "filename": "task_img/lp__cat1.png",
        "category": "cat",
        "manipulation": "low-pass"
    },
    {
        "": "398",
        "filename": "task_img/heavyclutter_bird7.png",
        "category": "bird",
        "manipulation": "clutter"
    },
    {
        "": "399",
        "filename": "task_img/control_tree1.png",
        "category": "tree",
        "manipulation": "control"
    }
]